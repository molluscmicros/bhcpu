# BHCPU

BHCPU is an attempt by Mollusc Micros Ltd. to design a RISC-V CPU suitable for microcontroller use over the 2019 late August bank holiday weekend.
BHCPU is designed to be a RV32IC[M] or RV32EC[M] core that can be implemented in a small FPGA.
The CPU has been developed publicly, hosted at https://bitbucket.org/molluscmicros/bhcpu

# Structure

* fw/ contains the firmware source code
* sw/ contains software
* doc/ contains documentation on the CPU design
* log/ contains logs of development activity

# License

The firmware, software and LaTex source files in this repository are released under the MIT license, see license.txt.
The documentation for the CPU design is released under a creative commons license (TBD).

