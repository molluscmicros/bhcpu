"""
This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcpu

Copyright 2019 Mollusc Micros Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("fn")
parser.add_argument("--bolilerplate", action="store_true")
args = parser.parse_args()


license_txt = "This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp\n\n"
with open("../license.txt", "r") as inp:
    license_txt += inp.read()

linecomments = {
    "py": "#",
    "cpp": "//",
    "h": "//",
     "vhd": "--"
}

blockcomments = {
    "py": ["\"\"\""],
    "cpp": ["/*", "*/"],
    "h": ["/*", "*/"]
}

boilerplate = {
}

filetype = args.fn.split(".")[-1]
assert filetype in linecomments or filetype in blockcomments

with open(args.fn, "w") as outp:
    if filetype in blockcomments:
	bc = blockcomments[filetype]
        outp.write(bc[0] + "\n")
	outp.write(license_txt)
        if len(bc) > 1:
            outp.write(bc[1] + "\n")
        else:
            outp.write(bc[0] + "\n")
    elif filetype in linecomments:
        lc = linecomments[filetype]
        license_txt = license_txt.split("\n")
        for line in license_txt:
            outp.write(lc + " " + line + "\n")
    if filetype in boilerplate:
        outp.write("\n")
        boilerplate[filetype](args.fn, outp)
    outp.write("\n")
    
print args.fn
