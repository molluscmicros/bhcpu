-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;
use work.bhcpu_pkg.all;

entity fetch_tb is

end entity fetch_tb;

architecture sim of fetch_tb is

-- infrastructure
signal clk 		: std_logic := '1';
signal rst 		: std_logic := '1';
signal rst_ctr	: unsigned(3 downto 0)
					:= to_unsigned(1, 4);

-- Memory
constant LOG2_MEM_SIZE	: natural := 8;
signal mem 			: data_vt(2**LOG2_MEM_SIZE-1 downto 0)
						:= (others => (others => '0'));
signal mem_filled 	: std_logic := '0';
signal mem_fill_addr	: unsigned(LOG2_MEM_SIZE-1 downto 0)
							:= to_unsigned(1, LOG2_MEM_SIZE);

-- DUT
signal flush		: std_logic := '0';
signal pc 			: unsigned(XLEN32-1 downto 0)
						:= to_unsigned(0, XLEN32);

signal mem_i 		: instr_mem_it
						:= (
							info => DEFAULT_MEM_INFO,
							addr_ready => '0',
							data => (others => '0'),
							data_valid => '0'
						);

signal mem_o 		: instr_mem_ot
						:= (
							addr => to_unsigned(0, XLEN32),
							addr_valid => '0',
							data_ready => '0'
						);

signal odata 		: std_logic_vector(XLEN32-1 downto 0)
						:= (others => '0');
signal odata_valid	: std_logic := '0';
signal odata_ready	: std_logic := '0';
signal shift16		: std_logic := '0';
signal err			: std_logic := '0';

-- testing
signal flushed		: std_logic := '0';

signal jump_ctr			: unsigned(7 downto 0)
							:= to_unsigned(0, 8);
constant JUMP_CTR_MAX	: unsigned(7 downto 0)
							:= to_unsigned(255, 8);
signal jump_addr 		: unsigned(XLEN32-1 downto 0)
							:= to_unsigned(14, XLEN32);

begin

	dut: entity work.fetch
	port map (
		clk => clk,
		rst => rst,

		flush => flush,
		pc => pc,

		mem_i => mem_i,
		mem_o => mem_o,

		odata => odata,
		odata_valid => odata_valid,
		odata_ready => odata_ready,
		shift16 => shift16,

		err => err
	);

	test: process(clk) is
	begin
		if rst = '0' and rising_edge(clk) then
			odata_ready <= '1';
			pc <= to_unsigned(16, pc'length); 
			if flushed = '0' then
				if flush = '0' then
					flush <= '1';
				else
					flushed <= '1';
					flush <= '0';
				end if;
			end if;

			if flushed = '1' then
				flush <= '0';
			end if;

			if mem_i.data_valid = '1' then
				if jump_ctr >= JUMP_CTR_MAX then
					jump_ctr <= to_unsigned(0, jump_ctr'length);
					flush <= '1';
					pc <= jump_addr;
					jump_addr <= jump_addr + 1;
				else
					jump_ctr <= jump_ctr + 1;
				end if;
			end if;


		end if; -- rst & clk
	end process test;

	instrmem: entity work.instr_mem_bram
	generic map (
		BASE_ADDR => to_unsigned(0, XLEN32),
		LOG2_MEM_SIZE => LOG2_MEM_SIZE
	) port map (
		clk => clk,

		mem_i => mem_i,
		mem_o => mem_o,

		buf => mem
	);

	fill: process(clk) is
	begin
		if rising_edge(clk) then
			if mem_filled = '0' then
				mem(to_integer(mem_fill_addr)) <= std_logic_vector(resize(mem_fill_addr, XLEN32));
				mem_fill_addr <= mem_fill_addr + 1;
				if mem_fill_addr = to_unsigned(0, mem_fill_addr'length) then
					mem_filled <= '1';
				end if;
			end if; -- not mem_filled

		end if; -- clk
	end process fill;

	tick: entity work.sim_tick
	generic map (
		PERIOD => 10 ns
	) port map (
		clk => clk
	);

	reset: process(clk) is
	begin
		if rising_edge(clk) and rst = '1' then
			if mem_filled = '1' then
				rst_ctr <= rst_ctr + 1;
				if rst_ctr = to_unsigned(0, rst_ctr'length) then
					rst <= '0';
				end if; -- rst_ctr
			end if; -- mem_filled
		end if; -- clk and rst
	end process reset;

end architecture sim;

