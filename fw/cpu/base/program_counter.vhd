-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;

entity program_counter is
	generic (
		RESET_ADDR	: unsigned(XLEN32-1 downto 0)
	);
	port (
		clk		: in std_logic;
		rst		: in std_logic;

		-- increment for either 32 or 16 bit instruction
		incr 		: in std_logic;
		compressed 	: in std_logic;

		-- jump to a new address
		jump	: in std_logic;
		jaddr	: in unsigned(XLEN32-1 downto 0);
		
		-- address and flush
		pc		: out unsigned(XLEN32-1 downto 0);
		flush	: out std_logic
	);

end entity program_counter;

architecture behavioural of program_counter is

signal pc_r 	: unsigned(XLEN32-1 downto 0) := RESET_ADDR;

begin
	
	pc <= pc_r;

	process(rst, clk) is
	begin
		if rst = '1' then
			pc_r <= RESET_ADDR;
			flush <= '1';
		elsif rising_edge(clk) then
			flush <= '0';
			if jump = '1' then
				pc_r <= jaddr;
				if compressed = '1' then
					if jaddr /= pc_r + 2 then
						flush <= '1';
					end if;
				else
					if jaddr /= pc_r + 4 then
						flush <= '1';
					end if;
				end if; 

			elsif incr = '1' then
				if compressed = '1' then
					pc_r <= pc_r + 2;
				else
					pc_r <= pc_r + 4;
				end if;
			end if;

		end if; -- rst/clk

	end process;


end architecture behavioural;
