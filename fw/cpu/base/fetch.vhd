-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;
use work.bhcpu_pkg.all;

entity fetch is
	port (
		clk		: in std_logic;
		rst		: in std_logic;

		flush	: in std_logic;
		pc		: in unsigned(XLEN32-1 downto 0);

		mem_i	: in instr_mem_it;
		mem_o	: out instr_mem_ot;

		odata		: out std_logic_vector(XLEN32-1 downto 0);
		odata_valid	: out std_logic;
		odata_ready	: in std_logic; 
		shift16		: out std_logic;

		err			: out std_logic
	);

end entity fetch;

architecture behavioural of fetch is

signal mem_info 		: mem_info_t
							:= DEFAULT_MEM_INFO;


signal odata_valid_r	: std_logic := '0';
signal addr_valid_r		: std_logic := '0';
signal idata_ready_r	: std_logic := '0';
signal addr				: unsigned(XLEN32-1 downto 0)
							:= to_unsigned(0, XLEN32);
signal addr_next		: unsigned(XLEN32-1 downto 0)
							:= to_unsigned(0, XLEN32);

signal data_r 			: std_logic_vector(XLEN32-1 downto 0)
							:= (others => '0');

signal pc_r				: unsigned(XLEN32-1 downto 0)
							:= to_unsigned(0, XLEN32);
						
type state_t is (CAP_INFO, MEM_CHECK, WAIT_FLUSH, FLUSHING, WAIT_IN, WAIT_OUT, ERR_TRAP);
signal state 	: state_t := MEM_CHECK;

begin

	odata_valid <= odata_valid_r;
	mem_o.data_ready <= idata_ready_r;
	mem_o.addr_valid <= addr_valid_r;
	mem_o.addr <= addr;
	odata <= data_r;
	
	process(clk, rst) is
	begin
		if rst = '1' then
			err <= '0';
			addr <= to_unsigned(0, XLEN32);
			state <= MEM_CHECK;
			mem_info <= mem_i.info;

		elsif rising_edge(clk) then

			addr_next <= addr + 4;
			case state is
			when CAP_INFO =>
				mem_info <= mem_i.info;
				state <= MEM_CHECK;

			when MEM_CHECK =>
				odata_valid_r <= '0';
				state <= WAIT_FLUSH;
				if mem_info.perm.x = '0' or
				   mem_info.base_addr = mem_info.max_addr then
					state <= ERR_TRAP;
				end if;

			when WAIT_FLUSH =>
				odata_valid_r <= '0';
				if flush = '1' then
					pc_r <= pc;
					state <= FLUSHING;
				end if;
					
			when FLUSHING =>
				odata_valid_r <= '0';
				idata_ready_r <= '0';
				if pc_r >= mem_info.base_addr and
				   		pc_r <= mem_info.max_addr and
				   		pc_r(0) = '0' then
					addr <= pc_r;
					addr_valid_r <= '1';
					state <= WAIT_IN;
					shift16 <= pc_r(1);
				else
					state <= ERR_TRAP;
				end if;

			when WAIT_IN =>
				odata_valid_r <= '0';
				idata_ready_r <= '1';

				if flush = '1' then
					pc_r <= pc;
					odata_valid_r <= '0';
					state <= FLUSHING;
				else
					if idata_ready_r = '1' and mem_i.data_valid = '1' then
						idata_ready_r <= '0';
						data_r <= mem_i.data;
						odata_valid_r <= '1';
						state <= WAIT_OUT;
						addr <= addr_next;
					end if;

				end if;

			when WAIT_OUT =>
				odata_valid_r <= '1';
				idata_ready_r <= '0';

				if flush = '1' then
					pc_r <= pc;
					odata_valid_r <= '0';
					state <= FLUSHING;
				else
					if odata_valid_r = '1' and odata_ready = '1' then
						shift16 <= '0';
						odata_valid_r <= '0';
						idata_ready_r <= '1';
						state <= WAIT_IN;
						if addr > mem_info.max_addr then
							state <= ERR_TRAP;
						end if;
					end if;

				end if;

			when ERR_TRAP =>
				err <= '1';

			end case state;

		end if; -- rst/clk

	end process;

end architecture behavioural;

