-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;

entity control_ex_tb is

end entity control_ex_tb;


architecture sim of control_ex_tb is
 
-- infrastructure
signal clk		: std_logic := '0';
signal rst		: std_logic := '1';
signal rst_ctr	: unsigned(3 downto 0)
					:= to_unsigned(1, 4);

-- dut
constant NREG		: natural := 32;
signal instr_valid	: std_logic := '0';
type instr_vt is array(natural range <>) of instr_t;
signal instrs		: instr_vt(7 downto 0)
			:= (
				0 => (opcode => JAL_OPC & INSTR32, rd => RA, rs1 => X0, rs2 => X0, funct3 => (others => '0'), funct7 => (others => '0'), imm => std_logic_vector(to_signed(-32, 32)), enc => J, compr => '0'),
			 	1 => (opcode => JALR_OPC & INSTR32, rd => RA, rs1 => X8, rs2 => X0, funct3 => (others => '0'), funct7 => (others => '0'), imm => std_logic_vector(to_signed(-32, 32)), enc => I, compr => '0'),
			 	2 => (opcode => BRANCH_OPC & INSTR32, rd => RA, rs1 => X8, rs2 => X9, funct3 => BEQ_FUNCT3, funct7 => (others => '0'), imm => std_logic_vector(to_signed(-32, 32)), enc => B, compr => '0'),
			 	3 => (opcode => BRANCH_OPC & INSTR32, rd => RA, rs1 => X8, rs2 => X9, funct3 => BNE_FUNCT3, funct7 => (others => '0'), imm => std_logic_vector(to_signed(-32, 32)), enc => B, compr => '0'),
			 	4 => (opcode => BRANCH_OPC & INSTR32, rd => RA, rs1 => X8, rs2 => X9, funct3 => BLT_FUNCT3, funct7 => (others => '0'), imm => std_logic_vector(to_signed(-32, 32)), enc => B, compr => '0'),
			 	5 => (opcode => BRANCH_OPC & INSTR32, rd => RA, rs1 => X8, rs2 => X9, funct3 => BGE_FUNCT3, funct7 => (others => '0'), imm => std_logic_vector(to_signed(-32, 32)), enc => B, compr => '0'),
			 	6 => (opcode => BRANCH_OPC & INSTR32, rd => RA, rs1 => X8, rs2 => X9, funct3 => BLTU_FUNCT3, funct7 => (others => '0'), imm => std_logic_vector(to_signed(-32, 32)), enc => B, compr => '0'),
			 	7 => (opcode => BRANCH_OPC & INSTR32, rd => RA, rs1 => X8, rs2 => X9, funct3 => BGEU_FUNCT3, funct7 => (others => '0'), imm => std_logic_vector(to_signed(-32, 32)), enc => B, compr => '0'));
			
					
signal instr		: instr_t;
signal instr_ctr	: unsigned(2 downto 0)
						:= to_unsigned(0, 3);
signal complete		: std_logic := '0';
signal regs 		: reg_vt(NREG-1 downto 0) 
			:= (
				1 => (others => '1'),
				8 => std_logic_vector(to_unsigned(55231, 32)),
				9 => std_logic_vector(to_unsigned(33103, 32)),
				others => (others => '0'));

signal pc			: unsigned(XLEN32-1 downto 0)
						:= to_unsigned(124132, XLEN32);
signal reg_outp_valid	: std_logic := '0';
signal reg_outp 		: std_logic_vector(XLEN32-1 downto 0) := (others => '0');
signal pc_outp_valid	: std_logic := '0';
signal pc_outp			: unsigned(XLEN32-1 downto 0)
							:= to_unsigned(0, XLEN32) - 1;
signal err				: std_logic := '0';

-- testing
type state_t is (INP, OUTP_WAIT, TICK);
signal state	: state_t := INP;

begin

	dut: entity work.control_ex
	generic map (
		NREG => NREG
	) port map (
		clk => clk,
		rst => rst,
		
		instr_valid => instr_valid,
		instr => instr,

		complete => complete,
		regs => regs,
		pc => pc,

		reg_outp_valid => reg_outp_valid,
		reg_outp => reg_outp,

		err => err
	);

	test: process(clk) is
	begin
		if rst = '1' then
			instr_valid <= '0';
			state <= INP;
			instr <= instrs(0);
		elsif rising_edge(clk) then
			case state is
			when INP => 
				instr_valid <= '1';
				state <= OUTP_WAIT;
				instr_ctr <= instr_ctr + 1;

			when OUTP_WAIT =>
				if complete = '1' or err = '1' then
					state <= TICk;
					instr_valid <= '0';
				end if;

			when TICK =>
				instr <= instrs(to_integer(instr_ctr));
				state <= INP;

			end case state;
			
		end if; -- clk

	end process test;


	st: entity work.sim_tick
	generic map (
		PERIOD => 10 ns
	) port map (
		clk => clk
	);

	reset: process(clk) is
	begin
		if rst = '1' and rising_edge(clk) then
			rst_ctr <= rst_ctr + 1;
			if rst_ctr = to_unsigned(0, rst_ctr'length) then
				rst <= '0';
			end if;
		end if;
	end process reset;

end architecture sim;

