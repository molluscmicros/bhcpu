-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;

entity decoder is
	port (
		clk			: in std_logic;
		rst			: in std_logic;

		-- data is padded to 32bit for compressed instructions
		data_ready	: out std_logic;
		data_valid	: in std_logic;
		data		: in std_logic_vector(XLEN32-1 downto 0);

		-- instruction
		err			: out std_logic;
		instr_valid : out std_logic;
		instr_ready	: in std_logic;
		instr		: out instr_t
	);



end entity decoder;

architecture behavioural of decoder is

type state_t is (WAIT_IN, WAIT_OUT, ERR_TRAP);
signal state	: state_t := WAIT_IN;

signal data_ready_r 	: std_logic := '0';
signal instr_valid_r 	: std_logic := '0';

begin
	data_ready <= data_ready_r;
	instr_valid <= instr_valid_r;

	process(clk,rst) is
	begin
		if rst = '1' then
			err <= '0';
			data_ready_r <= '0';
			instr_valid_r <= '0';
			state <= WAIT_IN;

		elsif rising_edge(clk) then
			err <= '0';
			data_ready_r <= '0';
			instr_valid_r <= '0';

			case state is
			when WAIT_IN =>
				data_ready_r <= '1';
				if data_valid = '1' and data_ready_r = '1' then
					instr_valid_r <= '1';
					state <= WAIT_OUT;
					data_ready_r <= '0';

					if data(4 downto 0) = "11111" then
						-- 48 bit or larger instruction, not supported
						state <= ERR_TRAP;

					elsif data(1 downto 0) = "11" then
						-- 32 bit instruction
						instr.opcode <= unsigned(data(6 downto 0));	
						instr.rd <= unsigned(data(11 downto 7));
						instr.rs1 <= unsigned(data(19 downto 15));
						instr.rs2 <= unsigned(data(24 downto 20));
						instr.funct3 <= unsigned(data(14 downto 12));
						instr.funct7 <= unsigned(data(31 downto 25));
						instr.compr <= '0';
						-- 01100  | R
						-- 00000, 00011, 00100, 11001, 11100  | I
						-- 01000  | S
						-- 11000  | B
						-- 00101, 01101  | U 
						-- 11011  | J
						if data(6 downto 2) = "01100" then
							-- R-type

							instr.enc <= R;

						elsif data(6 downto 2) = "00000" or
							  data(6 downto 2) = "00011" or
							  data(6 downto 2) = "00100" or
							  data(6 downto 2) = "11001" or
							  data(6 downto 2) = "11100" then
							-- I-type
							instr.rs2 <= to_unsigned(0, instr.rs2'length);
							instr.funct7 <= to_unsigned(0, instr.funct7'length);

							instr.imm(31 downto 11) <= (others => data(31));
							instr.imm(10 downto 0) <= data(30 downto 20);

							instr.enc <= I;

						elsif data(6 downto 2) = "01000" then
							-- S-type
							instr.rd <= to_unsigned(0, instr.rd'length);
							instr.rs2 <= to_unsigned(0, instr.rs2'length);
							instr.funct7 <= to_unsigned(0, instr.funct7'length);
							instr.imm(31 downto 11) <= (others => data(31));
							instr.imm(10 downto 5) <= data(30 downto 25);
							instr.imm(4 downto 0) <= data(11 downto 7);

							instr.enc <= S;


						elsif data(6 downto 2) = "11000" then
							-- B-type
							instr.rd <= to_unsigned(0, instr.rd'length);
							instr.funct7 <= to_unsigned(0, instr.funct7'length);

							instr.imm(31 downto 12) <= (others => data(31));
							instr.imm(11) <= data(7);
							instr.imm(10 downto 5) <= data(30 downto 25);
							instr.imm(4 downto 1) <= data(11 downto 8);
							instr.imm(0) <= '0';

							instr.enc <= B;

						elsif data(6 downto 2) = "00101" or
							  data(6 downto 2) = "01101" then
							-- U-type
							instr.rs1 <= to_unsigned(0, instr.rs1'length);
							instr.rs2 <= to_unsigned(0, instr.rs2'length);
							instr.funct3 <= to_unsigned(0, instr.funct3'length);
							instr.funct7 <= to_unsigned(0, instr.funct7'length);

							instr.imm(31 downto 12) <= data(31 downto 12);
							instr.imm(11 downto 0) <= (others => '0');
		
							instr.enc <= U;


						elsif data(6 downto 2) = "11011" then
							-- J-type
							instr.rs1 <= to_unsigned(0, instr.rs1'length);
							instr.rs2 <= to_unsigned(0, instr.rs2'length);
							instr.funct3 <= to_unsigned(0, instr.funct3'length);
							instr.funct7 <= to_unsigned(0, instr.funct7'length);

							instr.imm(31 downto 20) <= (others => data(31));
							instr.imm(19 downto 12) <= data(19 downto 12);
							instr.imm(11) <= data(20);
							instr.imm(10 downto 1) <= data(30 downto 21);
							instr.imm(0) <= '0';

							instr.enc <= J;

						else
							instr_valid_r <= '0';
							state <= ERR_TRAP;

						end if; -- 32b opcode
			

					else
						-- compressed instruction
						instr.compr <= '1';

					end if;
				end if;

			when WAIT_OUT =>
				instr_valid_r <= '1';
				if instr_valid_r = '1' and instr_ready = '1' then
					data_ready_r <= '1';
					state <= WAIT_IN;
					instr_valid_r <= '0';
				end if;
				
			when ERR_TRAP =>
				err <= '1';
				data_ready_r <= '0';
				instr_valid_r <= '0';

			end case state;


		end if; -- rst/clk
	end process;


end architecture behavioural;

