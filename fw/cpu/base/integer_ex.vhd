-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;

entity integer_ex is
	generic (
		NREG : natural := 32
	);
	port (
		clk		: in std_logic;
		rst		: in std_logic;

		instr_valid	: in std_logic;
		instr		: in instr_t;

		regs 		: in reg_vt(NREG-1 downto 0);
		pc			: in unsigned(XLEN32-1 downto 0);

		complete 		: out std_logic;

		reg_outp_valid	: out std_logic;
		reg_outp		: out std_logic_vector(XLEN32-1 downto 0);

		err		: out std_logic
	);
end entity integer_ex;

architecture behavioural of integer_ex is

type ex_instr_t is (ADDI, SLTI, SLTIU, ANDI, ORI, XORI, SLLI, SRLI, SRAI, LUI,
					AUIPC, ADD, SLT, SLTU, AND, OR, XOR, SLL, SRL, SUB, SRA);
signal ex_instr		: ex_instr_t := ADDI;

type state_t is (WAIT_IN, EX, WAIT_OUT, ERR_TRAP);
signal state	: state_t := WAIT_IN;

signal x_rs1	: std_logic_vector(XLEN32-1 downto 0);
signal x_rs2	: std_logic_vector(XLEN32-1 downto 0);

begin

	process(clk, rst) is
	begin
		if rst = '1' then
			reg_outp_valid <= '0';
			complete <= '0';
			state <= WAIT_IN;

		elsif rising_edge(clk) then

			case state is
			when WAIT_IN =>
				if instr_valid = '1' then
					state <= EX;
					x_rs1 <= regs(to_integer(instr.rs1));
					x_rs2 <= regs(to_integer(instr.rs2));

					if instr.opcode = OP_IMM_OPC & B"11" then
						if instr.funct3 = ADDI_FUNCT3 then
							ex_instr <= ADDI;
						elsif instr.funct3 = SLTI_FUNCT3 then
							ex_instr <= SLTI;
						elsif instr.funct3 = SLTUI_FUNCT3 then
							ex_instr <= SLTUI;
						elsif instr.funct3 = ANDI_FUNCT3 then
							ex_instr <= ANDI;
						elsif instr.funct3 = ORI_FUNCT3 then
							ex_instr <= ORI;
						elsif instr.funct3 = XORI_FUNCT3 then
							ex_instr <= XORI;
						elsif instr.funct3 = SLLI_FUNCT3 then
							ex_instr <= SLLI;
						elsif instr.funct3 = SRLI_FUNCT3 and instr.imm(30) = '0' then
							ex_instr <= SRLI;-
						elsif instr.funct3 = SRAI_FUNCT3 and instr.imm(30) = '1' then
							ex_instr <= SRAI;
						else
							state <= ERR_TRAP;
						end if; -- funct3

					elsif instr.opcode = OP_OPC & B"11" then
						if instr.funct3 = ADD_FUNCT3 and instr.funct7 = ADD_FUNCT7 then
							ex_instr <= ADD;
						elsif instr.funct3 = SLT_FUNCT3 and instr.funct7 = SLT_FUNCT7 then
							ex_instr <= SLT;
						elsif instr.funct3 = SLTU_FUNCT3 and instr.funct7 = SLTU_FUNCT7 then
							ex_instr <= SLTU;
						elsif instr.funct3 = AND_FUNCT3 and instr.funct7 = AND_FUNCT7 then
							ex_instr <= AND;
						elsif instr.funct3 = OR_FUNCT3 and instr.funct7 = OR_FUNCT7 then
							ex_instr <= OR;
						elsif instr.funct3 = XOR_FUNCT3 and instr.funct7 = XOR_FUNCT7 then
							ex_instr <= XOR;
						elsif instr.funct3 = SLL_FUNCT3 and instr.funct7 = SLL_FUNCT7 then
							ex_instr <= SLL;
						elsif instr.funct3 = SRL_FUNCT3 and instr.funct7 = SRL_FUNCT7 then
							ex_instr <= SRL;
						elsif instr.funct3 = SUB_FUNCT3 and instr.funct7 = SUB_FUNCT7 then
							ex_instr <= SUB;
						elsif instr.funct3 = SRA_FUNCT3 and instr.funct7 = SRA_FUNCT7 then
							ex_instr <= SRA;
						else
							state <= ERR_TRAP;
						end if;

					elsif instr.opcode = LUI_OPC & B"11" then
						ex_instr <= LUI;

					elsif instr.opcode = AUIPC & B"11" then
						ex_instr <= AUIPC;

					else
						state <= ERR_TRAP;
					end if; -- opcode

				end if;

			when EX =>
				state <= WAIT_OUT;
				reg_outp_valid <= '1';

				case ex_instr is
					when ADDI => 
						reg_outp <= std_logic_vector(signed(x_rs1) + signed(instr.imm));

					when SLTI => 
						reg_outp <= std_logic_vector(to_unsigned(0, XLEN32));
						if signed(x_rs1) < signed(instr.imm) then
							reg_outp <= std_logic_vector(to_unsigned(1, XLEN32));
						end if;

					when SLTIU => 
						reg_outp <= std_logic_vector(to_unsigned(0, XLEN32));
						if unsigned(x_rs1) < unsigned(instr.imm) then
							reg_outp <= std_logic_vector(to_unsigned(1, XLEN32));
						end if;

					when ANDI => 
						reg_outp <= x_rs1 and instr.imm;
						

					when ORI => 
						reg_outp <= x_rs1 or instr.imm;

					when XORI => 
						reg_outp <= x_rs1 xor instr.imm;

					when SLLI => 
						slligen: for i in 0 to 31 generate
							if unsigned(instr.imm(4 downto 0), 5) = to_unsigned(i, 5) then
								reg_outp(i-1 downto 0) <= (others => '0');
								reg_outp(XLEN32-1 downto i) <= x_rs1(XLEN32-i-1 downto 0);
							end if;

						end generate slligen;

					when SRLI => 
						srligen: for i in 0 to 31 generate
							if unsigned(instr.imm(4 downto 0), 5) = to_unsigned(i, 5) then
								reg_outp(XLEN32-1 downto XLEN32-i) downto 0) <= (others => '0');
								reg_outp(XLEN32-1 downto i) <= x_rs1(XLEN32-1 downto XLEN32-i);
							end if;

						end generate srligen;

					when SRAI => 
						sraigen: for i in 0 to 31 generate
							if unsigned(instr.imm(4 downto 0), 5) = to_unsigned(i, 5) then
								reg_outp(XLEN32-1 downto XLEN32-i) downto 0) <= (others => x_rs1(XLEN32-1));
								reg_outp(XLEN32-1 downto i) <= x_rs1(XLEN32-1 downto XLEN32-i);
							end if;

						end generate sraigen;

					when LUI => 
						reg_outp(XLEN32-1 downto 12) <= instr.imm(XLEN32-1 downto 12);
						reg_outp(11 downto 0) <= (others => '0');

					when AUIPC =>
						if signed(instr.imm) < 0 then
							reg_outp <= std_logic_vector(pc - unsigned(-signed(instr.imm)));
						else
							reg_outp <= std_logic_vector(pc + unsigned(instr.imm));

						end if;

					when ADD => 
						reg_outp => std_logic_vector(unsigned(x_rs1) + unsigned(x_rs2));

					when SLT => 
						reg_outp(XLEN31-1 downto 1) <= (others => '0');
						reg_outp(0) <= '0';
						if signed(x_rs1) < signed(x_rs2) then
							reg_outp(0) <= '1';
						end if;

					when SLTU => 
						reg_outp(XLEN31-1 downto 1) <= (others => '0');
						reg_outp(0) <= '0';
						if unsigned(x_rs1) < unsigned(x_rs2) then
							reg_outp(0) <= '1';
						end if;

					when AND => 
						reg_outp <= x_rs1 and x_rs2;

					when OR => 
						reg_outp <= x_rs1 or x_rs2;

					when XOR => 
						reg_outp <= x_rs1 xor x_rs2;

					when SLL => 
						sllgen: for i in 0 to 31 generate
							if unsigned(x_rst(4 downto 0), 5) = to_unsigned(i, 5) then
								reg_outp(i-1 downto 0) <= (others => '0');
								reg_outp(XLEN32-1 downto i) <= x_rs1(XLEN32-i-1 downto 0);
							end if;

						end generate sllgen;

					when SRL => 
						srlgen: for i in 0 to 31 generate
							if unsigned(x_rs2(4 downto 0), 5) = to_unsigned(i, 5) then
								reg_outp(XLEN32-1 downto XLEN32-i) downto 0) <= (others => '0');
								reg_outp(XLEN32-1 downto i) <= x_rs1(XLEN32-1 downto XLEN32-i);
							end if;

						end generate srlgen;

					when SUB => 
						reg_outp <= std_logic_vector(signed(x_rs1) - signed(x_rs2));

					when SRA => 
						sragen: for i in 0 to 31 generate
							if unsigned(x_rs2(4 downto 0), 5) = to_unsigned(i, 5) then
								reg_outp(XLEN32-1 downto XLEN32-i) downto 0) <= (others => x_rs1(XLEN32-1));
								reg_outp(XLEN32-1 downto i) <= x_rs1(XLEN32-1 downto XLEN32-i);
							end if;

						end generate sragen;


				end case ex_instr;

			when WAIT_OUT =>
				if instr_valid = '0' then
					reg_outp_valid <= '0';
					complete <= '0';
					state <= WAIT_IN;

				end if;

			when ERR_TRAP =>
				err <= '1';
				reg_outp_valid <= '0';
				complete <= '0';

			end case state;

		end if;

	end process;


end architecture behavioural;
	
