-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;

entity program_counter_tb is

end entity program_counter_tb;

architecture sim of program_counter_tb is

-- Infrastructure
signal clk			: std_logic := '1';
constant CLK_PERIOD	: time := 10 ns;
signal rst			: std_logic := '1';
signal rst_ctr		: unsigned(11 downto 0)
						:= to_unsigned(0, 12);

-- DUT

constant RESET_ADDR	: unsigned(XLEN32-1 downto 0)
						:= X"CAFE0000";
signal incr		: std_logic := '0';
signal compressed	: std_logic := '0';
signal jump 		: std_logic := '0';
signal jaddr		: unsigned(XLEN32-1 downto 0)
						:= to_unsigned(0, XLEN32);
signal pc			: unsigned(XLEN32-1 downto 0)
						:= to_unsigned(0, XLEN32);
signal flush		: std_logic := '0';

-- Testing
signal ctr	: unsigned(7 downto 0)
				:= to_unsigned(0, 8);

begin
	

	dut: entity work.program_counter
	generic map (
		RESET_ADDR => RESET_ADDR
	) port map (
		clk => clk,
		rst => rst,

		incr => incr,
		compressed => compressed,

		jump => jump,
		jaddr => jaddr,

		pc => pc,
		flush => flush
	);

	test: process(clk) is
	begin
		if rst = '1' then
			ctr <= to_unsigned(0, ctr'length);
		elsif rising_edge(clk) then
			ctr <= ctr + 1;
			incr <= ctr(2);
			compressed <= ctr(3);
			jump <= '0';
			if ctr(4 downto 0) = "10000" then
				jump <= '1'; 
				jaddr <= jaddr + 12;
			end if;

		end if; -- rst/clk

	end process test;

	reset: process(clk) is
	begin
		if rising_edge(clk) then
			rst <= '0';

			rst_ctr <= rst_ctr + 1;
			if rst_ctr < to_unsigned(8, rst_ctr'length) then
				rst <= '1';
			end if;
		end if; -- clk
	end process reset;

	tick: process is
	begin
		clk <= '0';
		wait for CLK_PERIOD / 2;
		clk <= '1';
		wait for CLK_PERIOD / 2;
	end process tick;

end architecture sim;
