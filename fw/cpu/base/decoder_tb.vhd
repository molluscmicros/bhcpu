-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;

entity decoder_tb is

end entity decoder_tb;

architecture sim of decoder_tb is

-- Infrastructure
constant TICK_PERIOD	: time := 10 ns;
signal clk				: std_logic := '1';
signal rst 				: std_logic := '1';
signal rst_ctr			: unsigned(3 downto 0)
							:= to_unsigned(1, 4);

-- Device under test
signal data_ready 		: std_logic := '0';
signal data_valid 		: std_logic := '0';
signal data				: std_logic_vector(XLEN32-1 downto 0);
signal err				: std_logic := '0';
signal instr_valid		: std_logic := '0';
signal instr_ready		: std_logic := '0';
signal instr			: instr_t
							:= (opcode => to_unsigned(0, 7),
								rd => to_unsigned(0, 5),
								rs1 => to_unsigned(0, 5),
								rs2 => to_unsigned(0, 5),
								funct3 => to_unsigned(0, 3),
								funct7 => to_unsigned(0, 7),
								imm => (others => '0'),
								enc => R,
								compr => '0');

-- testing
signal addi_rd  : unsigned(4 downto 0)
					:= to_unsigned(3, 5);
constant ADDI_RS1	: unsigned(4 downto 0)
						:= to_unsigned(7, 5);
constant ADDI_IMM : unsigned(11 downto 0)
					:= to_unsigned(123, 12);
			
signal addi_data	: std_logic_vector(31 downto 0)
						:= std_logic_vector(ADDI_IMM) &
						   std_logic_vector(ADDI_RS1) &
						   std_logic_vector(ADDI_FUNCT3) &
						   std_logic_vector(ADDI_RD) &
						   std_logic_vector(OP_IMM_OPC) &
						   "11";
		
							
							
signal data_ctr			: unsigned(3 downto 0) 
							:= to_unsigned(0, 4);
signal instr_ctr		: unsigned(2 downto 0) 
							:= to_unsigned(0, 3);
signal fullspeed_ctr	: unsigned(7 downto 0)
							:= to_unsigned(1, 8);
signal fullspeed		: std_logic := '0';

begin

	addi_data(11 downto 7) <= std_logic_vector(addi_rd);

	dut: entity work.decoder
	port map (
		clk => clk,
		rst => rst,

		data_ready => data_ready,
		data_valid => data_valid,
		data => data,

		err => err,
		instr_valid => instr_valid,
		instr_ready => instr_ready,
		instr => instr
	);

	testing: process(clk) is
	begin
		if rst = '1' then
			data_ctr <= to_unsigned(0, data_ctr'length);
			instr_ctr <= to_unsigned(0, instr_ctr'length);
			fullspeed_ctr <= to_unsigned(1, fullspeed_ctr'length);
			fullspeed <= '0';
		elsif rising_edge(clk) then
			data <= ADDI_DATA;

			if fullspeed = '1' then
				data_valid <= '1';
				instr_ready <= '1';
			else

				fullspeed_ctr <= fullspeed_ctr + 1;
				if fullspeed_ctr = to_unsigned(0, fullspeed_ctr'length) then
					fullspeed <= '1';
				end if;

				if data_valid = '1' then
					if data_ready = '1' then
						data_valid <= '0';
						addi_rd <= addi_rd + 1;
					end if;
				else
					data_ctr <= data_ctr + 1;
					if data_ctr = to_unsigned(0, data_ctr'length) then
						data_valid <= '1';
					end if;
				end if; -- data_valid

				instr_ctr <= instr_ctr + 1;
				instr_ready <= '0';
				if instr_ctr = to_unsigned(0, instr_ctr'length) then
					instr_ready <= '1';
				end if;
			end if; -- fullspeed
		end if; -- rst/clk

	end process;

	tick: entity work.sim_tick
	generic map (
		PERIOD => TICK_PERIOD
	) port map (
		clk => clk
	);

	reset: process(clk) is
	begin
		if rising_edge(clk)  and rst = '1' then
			rst_ctr <= rst_ctr + 1;
			if rst_ctr = to_unsigned(0, rst_ctr'length) then
				rst <= '0';
			end if;
		end if;
	end process;


end architecture sim;

