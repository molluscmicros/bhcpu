-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;

entity extractor is
	port (
		clk		: in std_logic;
		rst		: in std_logic;

		flush 		: in std_logic;
		shift16		: in std_logic;
		idata_ready	: out std_logic;
		idata_valid	: in std_logic;
		idata		: in std_logic_vector(XLEN32-1 downto 0);

		err			: out std_logic;
		odata_valid	: out std_logic;
		odata_ready : in std_logic;
		odata		: out std_logic_vector(XLEN32-1 downto 0)
	);

end entity extractor;

architecture behavioural of extractor is

constant HWLEN		: natural := XLEN32 / 2;
constant BUFLEN		: natural := XLEN32 + HWLEN;
type bufsize_t is (BUF0, BUF16, BUF32, BUF48);
signal bufsize 		: bufsize_t := BUF0;
type instrsize_t is (INSTR16, INSTR32);
signal instrsize 	: instrsize_t := INSTR32;

signal buf			: std_logic_vector(BUFLEN-1 downto 0);

type state_t is (WAIT_IN, EXTRACT, WAIT_OUT, ERR_TRAP);
signal state 	: state_t := WAIT_IN;

signal idata_ready_r	: std_logic := '0';
signal odata_valid_r	: std_logic := '0';

begin

	odata_valid <= odata_valid_r;
	idata_ready <= idata_ready_r;

	process(clk, rst) is
	begin
		if rst = '1' then
			bufsize <= BUF0;
			instrsize <= INSTR32;
			idata_ready_r <= '0';
			odata_valid_r <= '0';
			err <= '0';
			state <= WAIT_IN;

		elsif rising_edge(clk) then
			err <= '0';
			if flush = '1' then
				odata_valid_r <= '0';
				idata_ready_r <= '0';
				bufsize <= BUF0;
				instrsize <= INSTR32;
				if state /= ERR_TRAP then
					state <= WAIT_IN;
				end if;
			else

				case state is
				when WAIT_IN =>
					idata_ready_r <= '1';
					odata_valid_r  <= '0';
					if idata_ready_r = '1' and idata_valid = '1' then
						state <= EXTRACT;
						idata_ready_r <= '0';

						case BUFSIZE is
						when BUF0 =>
							if shift16 = '1' then
								BUFSIZE <= BUF16;
								buf(HWLEN-1 downto 0) <= idata(XLEN32-1 downto HWLEN);
								if idata(HWLEN+1 downto HWLEN) = B"11" then
									INSTRSIZE <= INSTR32;
									idata_ready_r <= '1';
									state <= WAIT_IN;
								else
									INSTRSIZE <= INSTR16;
								end if; 

							else
								BUFSIZE <= BUF32;
								buf(XLEN32-1 downto 0) <= idata;
								if idata(1 downto 0) = B"11" then
									INSTRSIZE <= INSTR32;
								else
									INSTRSIZE <= INSTR16;
								end if;
							end if; -- shift16

						when BUF16 =>
							if shift16 = '1' then
								BUFSIZE <= BUF32;
								buf(XLEN32-1 downto HWLEN) <= idata(XLEN32-1 downto HWLEN);
							else
								BUFSIZE <= BUF48;
								buf(BUFLEN-1 downto HWLEN) <= idata;
							end if; -- shift16

						when others =>
							-- Invalid state
							state <= ERR_TRAP;
							idata_ready_r <= '0';

						end case BUFSIZE;

					end if; -- ready and valid

				when EXTRACT =>
					idata_ready_r <= '0';
					odata_valid_r <= '1';
					state <= WAIT_OUT;

					case INSTRSIZE is
					when INSTR16 =>
						odata(HWLEN-1 downto 0) <= buf(HWLEN-1 downto 0);
						odata(XLEN32-1 downto HWLEN) <= (others => '0');
						case BUFSIZE is
						when BUF0 =>
							odata_valid_r <= '0';
							odata <= (others => '0');
							state <= ERR_TRAP;
						when BUF16 =>
							BUFSIZE <= BUF0;
						when BUF32 =>
							BUFSIZE <= BUF16;
						when BUF48 =>
							BUFSIZE <= BUF32;
						end case BUFSIZE;

					when INSTR32 =>
						odata <= buf(XLEN32-1 downto 0);
						case BUFSIZE is
						when BUF0 | BUF16 =>
							odata_valid_r <= '0';
							odata <= (others => '0');
							state <= ERR_TRAP;
						when BUF32 =>
							BUFSIZE <= BUF0;
						when BUF48 =>
							BUFSIZE <= BUF16;
						end case BUFSIZE;
					end case INSTRSIZE;

				when WAIT_OUT =>
					idata_ready_r <= '0';
					odata_valid_r <= '1';
					if odata_valid_r = '1' and odata_ready = '1' then
						odata_valid_r <= '0';
						case INSTRSIZE is
						when INSTR16 =>
							case BUFSIZE is
							when BUF0 =>
								state <= WAIT_IN;
								idata_ready_r <= '1';

							when BUF16 =>
								buf(HWLEN-1 downto 0) <= buf(XLEN32-1 downto HWLEN);
								if buf(HWLEN+1 downto HWLEN) = B"11" then
									state <= WAIT_IN;
									INSTRSIZE <= INSTR32;
									idata_ready_r <= '1';
								else
									state <= EXTRACT;
									INSTRSIZE <= INSTR16;
								end if;

							when BUF32 | BUF48 =>
								state <= ERR_TRAP;

							end case BUFSIZE;

						when INSTR32 =>
							case BUFSIZE is
							when BUF0 =>
								state <= WAIT_IN;
								idata_ready_r <= '1';

							when BUF16 =>
								buf(HWLEN-1 downto 0) <= buf(BUFLEN-1 downto XLEN32);
								if buf(XLEN32+1 downto XLEN32) = B"11" then
									state <= WAIT_IN;
									idata_ready_r <= '1';
								else
									state <= EXTRACT;
									INSTRSIZE <= INSTR16;
								end if;

							when BUF32 | BUF48 =>
								state <= ERR_TRAP;

							end case BUFSIZE;


						end case INSTRSIZE;

					end if;

				when ERR_TRAP =>
					err <= '1';
					idata_ready_r <= '0';
					odata_valid_r <= '0';

				end case state;


			end if; -- flush
		end if; --rst/clk

	end process;

end architecture behavioural;
		

