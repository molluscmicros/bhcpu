-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;

entity mem_ex is
	generic (
		NREG : natural := 32
	);
	port (
		clk		: in std_logic;
		rst		: in std_logic;

		instr_valid	: in std_logic;
		instr		: in instr_t;

		regs 		: in reg_vt(NREG-1 downto 0);

		complete 		: out std_logic;

		reg_outp_valid	: out std_logic;
		reg_outp		: out std_logic_vector(XLEN32-1 downto 0);

		err		: out std_logic
	);
end entity mem_ex;

architecture behavioural of mem_ex is

type state_t is (MEM_CHECK, WAIT_IN, ADDR_CHECK, L, S, WAIT_OUT, ERR_TRAP);
signal state	: state_t := MEM_CHECK;
signal mem_info	: mem_info_t
					:= DEFAULT_MEM_INFO;

signal ex_instr_t is (LOAD, STORE);
signal ex_instr		: ex_instr_t := LOAD;

type width_t is (B, BU, H, HU, W);
signal width := B;

signal x_rs2	: std_logic_vector(XLEN32-1 downto 0);

signal target_addr	: unsigned(XLEN32-1 downto 0);
						:= to_unsigned(0, XLEN32);
signal data			: std_logic_vector(XLEN32-1 downto 0)
						:= (others => '0');


signal tfr_ctr 	: unsigned(2 downto 0)
					:= to_unsigned(0, 3);

begin

	process(clk, rst) is
	begin
		if rst = '1' then
			reg_outp_valid <= '0';
			complete <= '0';
			state <= MEM_CHECK;
			mem_info <= mem_i.info;

		elsif rising_edge(clk) then

			case state is
			when MEM_CHECK =>
				complete <= '0';
				reg_outp_valid <= '0';
				state <= WAIT_IN;
				if mem_info.perms.r = '0' and mem_info.perms.w = '0' then
					state <= ERR_TRAP;
				elsif memo_info.base_addr = mem_info.max_addr then
					state <= ERR_TRAP;
				end if;


			when WAIT_IN =>
				complete <= '0';
				reg_outp_valid <= '0';
				state <= ADDR_CHECK;
				if instr_valid = '1' then
					state <= ADDR_CHECK;
					x_rs2 <= regs(to_integer(instr.rs2));

					if signed(instr.imm) < 0 then
						target_addr <= unsigned(regs(to_integer(instr.rs1))) - unsigned(-signed(instr.imm));
					else
						target_addr <= unsigned(regs(to_integer(instr.rs1))) + unsigned(instr.imm);
					end if;


					if instr.opcode = LOAD_OPC & B"11" then
						ex_instr <= LOAD;
					elsif instr.opcode = STORE_OPC & B"11" then
						ex_instr <= STORE;
					else
						state <= ERR_TRAP;
					end if; -- opcode

					if instr.funct3 = LB_FUNCT3 then
						width <= B;
					elsif instr.funct3 = LBU_FUNCT3 then
						width <= BU;
					elsif instr.funct3 = LH_FUNCT3 then
						width <= H;
					elsif instr.funct3 = LHU_FUNCT3 then
						width <= HU;
					elsif instr.funct3 = LW_FUNCT3 then
						width <= W;
					else
						state <= ERR_TRAP;
					end if; -- funct3
						

				end if;

			when ADDR_CHECK =>
				reg_outp_valid <= '0';
				complete <= '0';
				if ex_instr = STORE then
					state <= S;
				else
					state <= L;
				end if;

				mem_o.addr <= target_addr;
				mem_o.addr_valid <= '1';

				if ex_instr = STORE and (width = BU or width = HU) then
					state <= ERR_TRAP;
					mem_o.addr_valid <= '0';
				elsif ex_instr = STORE and mem_info.w = '0' then
					state <= ERR_TRAP;
					mem_o.addr_valid <= '0';
				elsif target_addr < mem_info.base_addr or target_addr > mem_info.max_addr then
					state <= ERR_TRAP;
					mem_o.addr_valid <= '0';
				end if;

				case width is
				when B | BU =>
					tfr_ctr <= to_unsigned(1, tfr_ctr'length);
				when H | HU =>
					tfr_ctr <= to_unsigned(2, tfr_ctr'length);
				when W =>
					tfr_ctr <= to_unsigned(4, tfr_ctr'length);
				end case width;
					
			when S =>
				reg_outp_valid <= '0';
				complete <= '0';

				if tfr_ctr = to_unsigned(0, tfr_ctr'length) then
					state <= WAIT_OUT;
				else

				end if;

			when L =>
				complete <= '0';
				reg_outp_valid <= '0';
				if tfr_ctr = to_unsigned(0, tfr_ctr'length) then
				
				else

				end if;
					

			when WAIT_OUT =>
				if instr_valid = '0' then
					reg_outp_valid <= '0';
					complete <= '0';
					state <= WAIT_IN;

				end if;

			when ERR_TRAP =>
				err <= '1';
				reg_outp_valid <= '0';
				complete <= '0';

			end case state;

		end if;

	end process;


end architecture behavioural;
	
