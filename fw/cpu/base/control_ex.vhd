-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;

entity control_ex is
	generic (
		NREG : natural := 32
	);
	port (
		clk		: in std_logic;
		rst		: in std_logic;

		instr_valid	: in std_logic;
		instr		: in instr_t;

		regs 		: in reg_vt(NREG-1 downto 0);
		pc			: in unsigned(XLEN32-1 downto 0);

		complete 		: out std_logic;

		reg_outp_valid	: out std_logic;
		reg_outp		: out std_logic_vector(XLEN32-1 downto 0);

		pc_outp_valid	: out std_logic;
		pc_outp			: out unsigned(XLEN32-1 downto 0);

		err		: out std_logic
	);
end entity control_ex;

architecture behavioural of control_ex is

type state_t is (WAIT_IN, EX, WAIT_OUT, ERR_TRAP);
signal state	: state_t := WAIT_IN;

type ex_instr_t is (JAL, JALR, B);
signal ex_instr	: ex_instr_t := JAL;

type bcond_t is (BEQ, BNE, BLT, BLTU, BGE, BGEU);
signal bcond	: bcond_t := BEQ;

signal x_rs1	: std_logic_vector(XLEN32-1 downto 0)
					:= (others => '0');
signal x_rs2	: std_logic_vector(XLEN32-1 downto 0)
					:= (others => '0');

begin

	process (clk, rst) is
	begin
		if rst = '1' then
			state <= WAIT_IN;
			err <= '0';
			complete <= '0';
			pc_outp_valid <= '0';
			reg_outp_valid <= '0';
		elsif rising_edge(clk) then
			complete <= '0';
			case state is
			when WAIT_IN =>
				if instr_valid = '1' then
					state <= EX;
					x_rs1 <= regs(to_integer(instr.rs1));
					x_rs2 <= regs(to_integer(instr.rs2));
					if instr.opcode = JAL_OPC & INSTR32 then
						ex_instr <= JAL;
					
					elsif instr.opcode = JALR_OPC & INSTR32 then
						ex_instr <= JALR;

					elsif instr.opcode = BRANCH_OPC & INSTR32 then
						ex_instr <= B;
						if instr.funct3 = BEQ_FUNCT3 then
							bcond <= BEQ;
						elsif instr.funct3 = BNE_FUNCT3 then
							bcond <= BNE;
						elsif instr.funct3 = BLT_FUNCT3 then
							bcond <= BLT;
						elsif instr.funct3 = BGE_FUNCT3 then
							bcond <= BGE;
						elsif instr.funct3 = BLTU_FUNCT3 then
							bcond <= BLTU;
						elsif instr.funct3 = BGEU_FUNCT3 then
							bcond <= BGEU;
						else
							state <= ERR_TRAP;
						end if; -- funct3

					else
						state <= ERR_TRAP;
					end if; -- opcode

				end if;

			when EX =>
				state <= WAIT_OUT;
				complete <= '1';
				case ex_instr is
				when JAL =>
					if signed(instr.imm) < 0 then
						pc_outp <= pc - unsigned(-signed(instr.imm));
					else
						pc_outp <= pc + unsigned(instr.imm);
					end if;
					pc_outp_valid <= '1';
					if instr.compr = '1' then
						reg_outp <= std_logic_vector(pc + 2);
					else
						reg_outp <= std_logic_vector(pc + 4);
					end if;
					reg_outp_valid <= '1';

				when JALR =>
					pc_outp <= unsigned(
								signed(x_rs1) +
							   	signed(instr.imm)
							   );
					pc_outp(0) <= '0';
					pc_outp_valid <= '1';
					if instr.compr = '1' then
						reg_outp <= std_logic_vector(pc + 2);
					else
						reg_outp <= std_logic_vector(pc + 4);
					end if;
					reg_outp_valid <= '1';
					
				when B =>

					pc_outp_valid <= '0';
					reg_outp_valid <= '0';
					if (bcond = BEQ and x_rs1 = x_rs2) or
					   (bcond = BNE and x_rs1 /= x_rs2) or
					   (bcond = BLT and signed(x_rs1) < signed(x_rs2)) or
					   (bcond = BLTU and unsigned(x_rs1) < unsigned(x_rs2)) or
					   (bcond = BGE and signed(x_rs1) >= signed(x_rs2)) or
					   (bcond = BGEU and unsigned(x_rs1) >= unsigned(x_rs1)) then
						if signed(instr.imm) < 0 then
							pc_outp <= pc - unsigned(-signed(instr.imm));
						else
							pc_outp <= pc + unsigned(instr.imm);
						end if;
						pc_outp_valid <= '1';
					end if;

				end case ex_instr;

			when WAIT_OUT =>
				complete <= '1';
				if instr_valid = '0' then
					state <= WAIT_IN;
					complete <= '0';
					pc_outp_valid <= '0';
					reg_outp_valid <= '0';
				end if;

			when ERR_TRAP =>
				complete <= '0';
				err <= '1';
				pc_outp_valid <= '0';
				reg_outp_valid <= '0';

			end case state;
				

		end if; -- rst/clk
	end process;

end architecture behavioural;
	
