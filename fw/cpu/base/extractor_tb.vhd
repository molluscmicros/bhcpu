-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;

entity extractor_tb is

end entity extractor_tb;

architecture sim of extractor_tb is

-- Infrastructure
signal clk		: std_logic := '1';
signal rst		: std_logic := '0';

-- Device under test
signal flush		: std_logic := '0';
signal shift16		: std_logic := '0';
signal idata_ready 	: std_logic := '0';
signal idata_valid	: std_logic := '0';
signal idata		: std_logic_vector(XLEN32-1 downto 0) := (others => '0');
signal err			: std_logic := '0';
signal odata_valid	: std_logic := '0';
signal odata_ready 	: std_logic := '0';
signal odata		: std_logic_vector(XLEN32-1 downto 0) := (others => '0');

-- Instruction data
type data_vt is array(natural range <>) of std_logic_vector(XLEN32-1 downto 0);
constant NWORD	: natural := 16;
-- W00, HW01, HW02, HW03, W04, W05, HW06, HW07, HW08
-- HW09, W0a, HW0b, W0c, W0d, W0e, W0f, W10, HW11,
-- HW12, W13, HW14, HW15
constant data	: data_vt(NWORD-1 downto 0)
					:= (0 => X"00ee_0003", -- W00
					    1 => X"0201_0101", -- HW02, HW01
					    2 => X"0403_0301", -- WL04, HW03
					    3 => X"0503_04ee", -- WL05, WU04
					    4 => X"0601_05ee", -- HW06, WU05
					    5 => X"0801_0701", -- HW08, HW07
					    6 => X"0a03_0901", -- WL0A, HW09
					    7 => X"0b01_0aee", -- HW0B, WU0A
					    8 => X"0cee_0c03", -- W0C
					    9 => X"0dee_0d03", -- W0D
					   10 => X"0eee_0e03", -- W0E
					   11 => X"0fee_0f03", -- W0F
					   12 => X"10ee_1003", -- W10
					   13 => X"1201_1101", -- HW12, HW11
					   14 => X"13ee_1303", -- W13
					   15 => X"1501_1401"); -- HW15, HW14
signal word_ctr		: unsigned(3 downto 0)
						:= to_unsigned(0, 4);

signal jump_ctr			: unsigned(7 downto 0)
							:= to_unsigned(0, 8);
constant JUMP_CTR_MAX	: unsigned(7 downto 0)
							:= to_unsigned(123, 8);
signal jump_w			: std_logic := '0';
constant JUMP_ADDR_W	: unsigned(3 downto 0)
							:= to_unsigned(9, 4);
constant JUMP_ADDR_HW	: unsigned(3 downto 0)
							:= to_unsigned(4, 4);

signal ready_ctr		: unsigned(7 downto 0)
							:= to_unsigned(0, 8);
constant READY_DUTY		: unsigned(7 downto 0)
							:= to_unsigned(230, 8);
					
begin

	dut: entity work.extractor
	port map (
		clk => clk,
		rst => rst,
	
		flush => flush,
		shift16 => shift16,
		idata_ready => idata_ready,
		idata_valid => idata_valid,
		idata => idata,

		err => err,
		odata_valid => odata_valid,
		odata_ready => odata_ready,
		odata => odata
	);

	test: process(clk) is
	begin
		if rising_edge(clk) then 
			odata_ready <= '1';
			ready_ctr <= ready_ctr + 1;
			if ready_ctr >= READY_DUTY then
				odata_ready <= '0';
			end if;

			idata <= data(to_integer(word_ctr));
			idata_valid <= '1';
			flush <= '0';
			if idata_ready = '1' then
				if shift16 = '1' then
					shift16 <= '0';
				end if;

				word_ctr <= word_ctr + 1;
				if jump_ctr >= JUMP_CTR_MAX then
					jump_ctr <= to_unsigned(0, jump_ctr'length);
					flush <= '1';
					if jump_w = '1' then
						word_ctr <= JUMP_ADDR_W;
						jump_w <= '0';
					else
						word_ctr <= JUMP_ADDR_HW;
						jump_w <= '1';
						shift16 <= '1';
					end if;
				else
					jump_ctr <= jump_ctr + 1;
				end if;
			end if;
			

		end if; -- clk
	end process test;

	tick: entity work.sim_tick
	generic map (
		PERIOD => 10 ns
	) port map (
		clk => clk
	);

end architecture sim;

