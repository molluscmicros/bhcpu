-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;

use work.bhcpu_pkg.all;

entity instr_mem_bram is
	generic (
		BASE_ADDR		: unsigned(XLEN32-1 downto 0);
		LOG2_MEM_SIZE	: natural
	);
	port (
		clk		: in std_logic;

		mem_i	: out instr_mem_it;
		mem_o	: in instr_mem_ot;

		buf		: in data_vt(2**LOG2_MEM_SIZE-1 downto 0)
	);

end entity instr_mem_bram;

architecture behavioural of instr_mem_bram is

constant MEM_INFO	: mem_info_t
						:= (
							base_addr => BASE_ADDR,
							max_addr => BASE_ADDR + to_unsigned(2**(LOG2_MEM_SIZE+1)-1, XLEN32),
							perm => (r => '0', w=>'0', x => '1')
						);

signal addr		: unsigned(LOG2_MEM_SIZE-1 downto 0)
					:= to_unsigned(0, LOG2_MEM_SIZE);
signal iaddr_prev	: unsigned(XLEN32-1 downto 0)
						:= to_unsigned(0, XLEN32);
signal addr_valid_prev		: std_logic := '0';


begin

	mem_i.info <= MEM_INFO;
	addr <= mem_o.addr(LOG2_MEM_SIZE+1 downto 2);

	process(clk) is
	begin
		if rising_edge(clk) then
			mem_i.addr_ready <= '1';
			mem_i.data <= buf(to_integer(addr));
			addr_valid_prev <= mem_o.addr_valid;
			if mem_o.addr_valid <= '1' then
				iaddr_prev <= mem_o.addr;
			end if;

			mem_i.data_valid <= '0';
			if mem_o.addr_valid = '1' and addr_valid_prev = '1' and iaddr_prev = mem_o.addr then
				mem_i.data_valid <= '1';
			end if;
		end if; -- clk

	end process;

end architecture behavioural;

