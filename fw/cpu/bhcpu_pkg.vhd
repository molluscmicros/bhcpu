-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_pkg.all;

package bhcpu_pkg is

	type data_vt is array(natural range <>) of std_logic_vector(XLEN32-1 downto 0);

	type byte_vt is array(natural range <>) of std_logic_vector(7 downto 0);

	type perm_t is record
		r	: std_logic;
		w	: std_logic;
		x	: std_logic;
	end record perm_t;

	constant NOPERMS	: perm_t 
							:= (r => '0', w => '0', x => '0');
	type mem_info_t is record
		base_addr	: unsigned(XLEN32-1 downto 0);
		max_addr	: unsigned(XLEN32-1 downto 0);
		perm		: perm_t;
	end record mem_info_t;

	constant DEFAULT_MEM_INFO	: mem_info_t
					:= (base_addr => to_unsigned(0, XLEN32),
						max_addr => to_unsigned(0, XLEN32),
					 	perm => NOPERMS);

	type instr_mem_it is record
		info		: mem_info_t;

		addr_ready	: std_logic;

		data		: std_logic_vector(XLEN32-1 downto 0);
		data_valid	: std_logic;
	end record instr_mem_it;

	type instr_mem_ot is record
		addr		: unsigned(XLEN32-1 downto 0);
		addr_valid	: std_logic;

		data_ready 	: std_logic;
	end record instr_mem_ot;

	type data_mem_it is record
		info 		: mem_info_t;

		addr_ready	: std_logic;
	
		rdata		: std_logic_vector(XLEN32-1 downto 0);
		rdata_valid	: std_logic_vector(XLEN32-1 downto 0);
	end record data_mem_it;

	type data_mem_ot is record
		addr		: unsigned(XLEN32-1 downto 0);
		addr_valid	: std_logic;
		
		wdata		: std_logic_vector(XLEN32-1 downto 0);
		wen			: std_logic;

		data_ready : std_logic;
	end record data_mem_ot;

	

end package bhcpu_pkg;
