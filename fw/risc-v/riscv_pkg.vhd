-- This file is part of BHCPU, see https://bitbucket.org/molluscmicros/bhcp
-- 
-- Copyright 2019 Mollusc Micros Ltd.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package riscv_pkg is

	constant XLEN32	: natural := 32;

	-- RV32I op codes
	constant LOAD_OPC		: std_logic_vector(4 downto 0) := "00000";
	constant LOAD_FP_OPC	: std_logic_vector(4 downto 0) := "00001";
	constant CUSTOM0_OPC	: std_logic_vector(4 downto 0) := "00010";
	constant MISC_MEM_OPC	: std_logic_vector(4 downto 0) := "00011";
	constant OP_IMM_OPC		: std_logic_vector(4 downto 0) := "00100";
	constant AUIPC_OPC		: std_logic_vector(4 downto 0) := "00101";
	constant OP_IMM32_OPC	: std_logic_vector(4 downto 0) := "00110";
	constant STORE_OPC		: std_logic_vector(4 downto 0) := "01000";
	constant STORE_FP_OPC	: std_logic_vector(4 downto 0) := "01001";
	constant CUSTOM1_OPC	: std_logic_vector(4 downto 0) := "01010";
	constant AMO_OPC		: std_logic_vector(4 downto 0) := "01011";
	constant OP_OPC			: std_logic_vector(4 downto 0) := "01100";
	constant LUI_OPC		: std_logic_vector(4 downto 0) := "01101";
	constant OP32_OPC		: std_logic_vector(4 downto 0) := "01110";
	constant MADD_OPC		: std_logic_vector(4 downto 0) := "10000";
	constant MSUB_OPC		: std_logic_vector(4 downto 0) := "10001";
	constant NMSUB_OPC		: std_logic_vector(4 downto 0) := "10010";
	constant NMADD_OPC		: std_logic_vector(4 downto 0) := "10011";
	constant OP_FP_OPC		: std_logic_vector(4 downto 0) := "10100";
	constant RSVD0_OPC		: std_logic_vector(4 downto 0) := "10101";
	constant CUSTOM2_OPC	: std_logic_vector(4 downto 0) := "10110";
	constant BRANCH_OPC		: std_logic_vector(4 downto 0) := "11000";
	constant JALR_OPC		: std_logic_vector(4 downto 0) := "11001";
	constant RSVD1_OPC		: std_logic_vector(4 downto 0) := "11010";
	constant JAL_OPC		: std_logic_vector(4 downto 0) := "11011";
	constant SYSTEM_OPC		: std_logic_vector(4 downto 0) := "11100";
	constant RSVD3_OPC		: std_logic_vector(4 downto 0) := "11101";
	constant CUSTOM3_OPC	: std_logic_vector(4 downto 0) := "11110";

	constant JALR_FUNCT3	: std_logic_vector(2 downto 0) := "000";

	constant BEQ_FUNCT3		: std_logic_vector(2 downto 0) := "000";
	constant BNE_FUNCT3		: std_logic_vector(2 downto 0) := "001";
	constant BLT_FUNCT3		: std_logic_vector(2 downto 0) := "100";
	constant BGE_FUNCT3		: std_logic_vector(2 downto 0) := "101";
	constant BLTU_FUNCT3	: std_logic_vector(2 downto 0) := "110";
	constant BGEU_FUNCT3	: std_logic_vector(2 downto 0) := "111";

	constant LB_FUNCT3		: std_logic_vector(2 downto 0) := "000";
	constant LH_FUNCT3		: std_logic_vector(2 downto 0) := "001";
	constant LW_FUNCT3		: std_logic_vector(2 downto 0) := "010";
	constant LBU_FUNCT3		: std_logic_vector(2 downto 0) := "100";
	constant LHU_FUNCT3		: std_logic_vector(2 downto 0) := "101";

	constant ADDI_FUNCT3	: std_logic_vector(2 downto 0) := "000";
	constant SLTI_FUNCT3	: std_logic_vector(2 downto 0) := "010";
	constant SLTIU_FUNCT3	: std_logic_vector(2 downto 0) := "011";
	constant XORI_FUNCT3	: std_logic_vector(2 downto 0) := "100";
	constant ORI_FUNCT3		: std_logic_vector(2 downto 0) := "110";
	constant ANDI_FUNCT3	: std_logic_vector(2 downto 0) := "111";
	constant SLLI_FUNCT3	: std_logic_vector(2 downto 0) := "001";
	constant SRLI_FUNCT3	: std_logic_vector(2 downto 0) := "101";
	constant SRAI_FUNCT3	: std_logic_vector(2 downto 0) := "101";

	constant ADD_FUNCT3		: std_logic_vector(2 downto 0) := "000";
	constant SUB_FUNCT3		: std_logic_vector(2 downto 0) := "000";
	constant SLL_FUNCT3		: std_logic_vector(2 downto 0) := "001";
	constant SLT_FUNCT3		: std_logic_vector(2 downto 0) := "010";
	constant SLTU_FUNCT3	: std_logic_vector(2 downto 0) := "011";
	constant XOR_FUNCT3		: std_logic_vector(2 downto 0) := "100";
	constant SRL_FUNCT3		: std_logic_vector(2 downto 0) := "101";
	constant SRA_FUNCT3		: std_logic_vector(2 downto 0) := "100";
	constant OR_FUNCT3		: std_logic_vector(2 downto 0) := "110";
	constant AND_FUNCT3		: std_logic_vector(2 downto 0) := "111";

	constant ADD_FUNCT7		: std_logic_vector(6 downto 0) := "0000000";
	constant SUB_FUNCT7		: std_logic_vector(6 downto 0) := "0100000";
	constant SLL_FUNCT7		: std_logic_vector(6 downto 0) := "0000000";
	constant SLT_FUNCT7		: std_logic_vector(6 downto 0) := "0000000";
	constant SLTU_FUNCT7	: std_logic_vector(6 downto 0) := "0000000";
	constant XOR_FUNCT7		: std_logic_vector(6 downto 0) := "0000000";
	constant SRL_FUNCT7		: std_logic_vector(6 downto 0) := "0000000";
	constant SRA_FUNCT7		: std_logic_vector(6 downto 0) := "0100000";
	constant OR_FUNCT7		: std_logic_vector(6 downto 0) := "0000000";
	constant AND_FUNCT7		: std_logic_vector(6 downto 0) := "0000000";

	constant X0		: unsigned(4 downto 0)
						:= to_unsigned(0, 5);
	constant X1		: unsigned(4 downto 0)
						:= to_unsigned(1, 5);
	constant X2		: unsigned(4 downto 0)
						:= to_unsigned(2, 5);
	constant X3		: unsigned(4 downto 0)
						:= to_unsigned(3, 5);
	constant X4		: unsigned(4 downto 0)
						:= to_unsigned(4, 5);
	constant X5		: unsigned(4 downto 0)
						:= to_unsigned(5, 5);
	constant X6		: unsigned(4 downto 0)
						:= to_unsigned(6, 5);
	constant X7		: unsigned(4 downto 0)
						:= to_unsigned(7, 5);
	constant X8		: unsigned(4 downto 0)
						:= to_unsigned(8, 5);
	constant X9		: unsigned(4 downto 0)
						:= to_unsigned(9, 5);
	constant X10	: unsigned(4 downto 0)
						:= to_unsigned(10, 5);
	constant X11	: unsigned(4 downto 0)
						:= to_unsigned(11, 5);
	constant X12	: unsigned(4 downto 0)
						:= to_unsigned(12, 5);
	constant X13	: unsigned(4 downto 0)
						:= to_unsigned(13, 5);
	constant X14	: unsigned(4 downto 0)
						:= to_unsigned(14, 5);
	constant X15	: unsigned(4 downto 0)
						:= to_unsigned(15, 5);
	constant X16	: unsigned(4 downto 0)
						:= to_unsigned(16, 5);
	constant X17	: unsigned(4 downto 0)
						:= to_unsigned(17, 5);
	constant X18	: unsigned(4 downto 0)
						:= to_unsigned(18, 5);
	constant X19	: unsigned(4 downto 0)
						:= to_unsigned(19, 5);
	constant X20	: unsigned(4 downto 0)
						:= to_unsigned(20, 5);
	constant X21	: unsigned(4 downto 0)
						:= to_unsigned(21, 5);
	constant X22	: unsigned(4 downto 0)
						:= to_unsigned(22, 5);
	constant X23	: unsigned(4 downto 0)
						:= to_unsigned(23, 5);
	constant X24	: unsigned(4 downto 0)
						:= to_unsigned(24, 5);
	constant X25	: unsigned(4 downto 0)
						:= to_unsigned(25, 5);
	constant X26	: unsigned(4 downto 0)
						:= to_unsigned(26, 5);
	constant X27	: unsigned(4 downto 0)
						:= to_unsigned(27, 5);
	constant X28	: unsigned(4 downto 0)
						:= to_unsigned(28, 5);
	constant X29	: unsigned(4 downto 0)
						:= to_unsigned(29, 5);
	constant X30	: unsigned(4 downto 0)
						:= to_unsigned(30, 5);
	constant X31	: unsigned(4 downto 0)
						:= to_unsigned(31, 5);

	constant RA		: unsigned(4 downto 0)
						:= X1;
	constant SP		: unsigned(4 downto 0)
						:= X2;

	constant INSTR32	: std_logic_vector(1 downto 0) := B"11";

	type encoding_t is (R, I, S, B, U, J, CR, CI, CSS, CIW, CL, CS, CA, CB, CJ);

	type instr_t is record
		opcode	: std_logic_vector(6 downto 0);
		rd		: unsigned(4 downto 0);
		rs1		: unsigned(4 downto 0);
		rs2 	: unsigned(4 downto 0);
		funct3	: std_logic_vector(2 downto 0);
		funct7	: std_logic_vector(6 downto 0);
		imm		: std_logic_vector(31 downto 0);
		enc		: encoding_t;
		compr	: std_logic;
	end record instr_t;

	type reg_vt is array(natural range <>) of std_logic_vector(XLEN32-1 downto 0);

end package riscv_pkg;

